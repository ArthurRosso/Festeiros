package br.com.crudfest.rosso.festeiros.model;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Created by Arthur Rosso on 24/11/2017.
 */

public class Binding {

	public static ArrayList<Festa> FindFesta () {

		ArrayList<Festa> f;

		try {
			InputStream fos = new FileInputStream("festa");
			ObjectInputStream ois = new ObjectInputStream(fos);
			f = (ArrayList<Festa>) ois.readObject();
			fos.close();
			ois.close();

		} catch (Exception e) {
			// e.printStackTrace();
			return new ArrayList<Festa>();
		}

		return f;
	}

}
