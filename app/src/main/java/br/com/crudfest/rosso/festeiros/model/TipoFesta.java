package br.com.crudfest.rosso.festeiros.model;

/**
 *
 * @author ArthurRosso
 */
public class TipoFesta {
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }   
}
