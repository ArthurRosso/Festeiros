package br.com.crudfest.rosso.festeiros.model;

import java.io.Serializable;

/**
 *
 * @author ArthurRosso
 */
public class Festa implements Serializable {

	private String tipoFesta;
	private String nome;
	public int maxIngressos;
	public int numIngressos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getMaxIngressos() {
		return maxIngressos;
	}

	public void setMaxIngressos(int maxIngressos) {
		this.maxIngressos = maxIngressos;
	}

	public int getNumIngressos() {
		return numIngressos;
	}

	public void setNumIngressos(int numIngressos) {
		this.numIngressos = numIngressos;
	}

	public Ingresso comprar(Cliente cliente) {

		if (numIngressos > 0) {
			Ingresso i = new Ingresso(cliente, this);
			numIngressos--;
			return i;
		}

		return null;
	}
}
