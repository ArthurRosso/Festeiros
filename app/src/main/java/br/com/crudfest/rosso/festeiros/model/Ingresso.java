package br.com.crudfest.rosso.festeiros.model;

/**
 *
 * @author Aluno
 */
public class Ingresso {
    private int codigo;
    private Festa festa;
    private Cliente cliente;


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Ingresso(Cliente cliente, Festa festa) {
        this.cliente = cliente;
        this.festa = festa;

        this.codigo = festa.getMaxIngressos() - festa.getNumIngressos();

        cliente.addIngresso(this);
    }

}
