package br.com.crudfest.rosso.festeiros;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	}

	/**
	 * Evento criado para servir como método de listener.
	 *
	 * @param v
	 */
	public void comprarIngresso(View v) {

		Context contexto = getApplicationContext();
		Intent objIntent = new Intent(contexto, CompraActivity.class);

		startActivity(objIntent);
	}
}
