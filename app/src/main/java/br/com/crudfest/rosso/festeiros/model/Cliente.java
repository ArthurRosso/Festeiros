package br.com.crudfest.rosso.festeiros.model;

import java.util.ArrayList;

/**
 *
 * @author ArthurRosso
 */
public class Cliente {
    private String nome;
    private char sexo;
    private String email;
    private int pontos;

    private ArrayList<Ingresso> ingressos = new ArrayList<>();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPontinhos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public ArrayList<Ingresso> getIngressos() {
        return ingressos;
    }

    public void setIngressos(ArrayList<Ingresso> ingressos) {
        this.ingressos = ingressos;
    }

    public Cliente(String nome) {
        this.nome = nome;
        pontos = 0;
    }

    public void addIngresso(Ingresso i) {
        pontos++;
        this.ingressos.add(i);
    }

}
